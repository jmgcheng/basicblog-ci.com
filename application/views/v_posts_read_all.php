<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Basic Blog</title>
	</head>
	<body>
		<header>
			<h1>
				<a href="<?php echo base_url(); ?>">
					Basic Blog
				</a>
			</h1>
			<p>
				Hello, 
				<?php
					$a_user_details = $this->session->userdata('a_user_details');
					if( isset($a_user_details['s_u_username']) && !empty($a_user_details['s_u_username']) ):
						echo $a_user_details['s_u_username'] .'.';
				?>
						<br/><a href="<?php echo base_url(); ?>user/logout">Logout</a>
						<br/><a href="<?php echo base_url(); ?>post/create_form">Create Post</a>
						<br/><a href="<?php echo base_url(); ?>post/read_all">Search Post</a>
				<?php
					else:
				?>
						Guest. Kindly
						<a href="<?php echo base_url(); ?>user/login_form">
							Login
						</a>
						or 
						<a href="<?php echo base_url(); ?>user/register_form">
							Register
						</a>
				<?php
					endif;
				?>
			</p>
		</header>
		<div>
			<section>
				<header>
					<h2>
						Browse Posts
					</h2>
				</header>
				<form id="frm_post_search" name="frm_post_search" action="<?php echo base_url(); ?>post/read_all" method="post">
					<label for="txt_user_registration_username">
						Title:
					</label>
					<input type="text" id="txt_post_search_title" name="txt_post_search_title" />
					<?php if( isset($a_form_notice['s_txt_post_search_title_error']) && !empty($a_form_notice['s_txt_post_search_title_error']) ) : ?>
						<p><?php echo $a_form_notice['s_txt_post_search_title_error']; ?></p>
					<?php endif; ?>
					<br/>
					<input type="submit" value="Search" />
					<br/>
					<?php if( isset($a_form_notice['a_site_response_info']) && !empty($a_form_notice['a_site_response_info']) ) : ?>
						<ul>
						<?php
							foreach( $a_form_notice['a_site_response_info'] AS $s_site_response_info ):
						?>
							<li>
								<?php
									echo $s_site_response_info;
								?>
							</li>
						<?php
							endforeach;
						?>
						</ul>
					<?php endif; ?>	
				</form>
			</section>
			
			<section>
				<header>
					Posts
				</header>
				<?php
					if(isset($a_post_results) && !empty($a_post_results)) :
						foreach( $a_post_results AS $a_post_details ):
				?>
						<div>
							<h3>
								<a href="<?php echo base_url(); ?>post/<?php echo $a_post_details['s_p_slug']; ?>">
									<?php
										echo $a_post_details['s_p_title'];
									?>
								</a>
							</h3>
							<p>
								<?php
									echo $a_post_details['s_p_content'];
								?>
							</p>
						</div>
				<?php
						endforeach;
					endif;
				?>
				<nav>
					<?php
						if( isset($s_page_links_pagination) && !empty($s_page_links_pagination) )
						{
							echo $s_page_links_pagination;
						}
					?>
				</nav>
			</section>
		</div>
		<footer>
		</footer>
	</body>
</html>