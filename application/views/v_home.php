<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Basic Blog</title>
	</head>
	<body>
		<header>
			<h1>
				<a href="<?php echo base_url(); ?>">
					Basic Blog
				</a>
			</h1>
			<p>
				Hello, 
				<?php
					$a_user_details = $this->session->userdata('a_user_details');
					if( isset($a_user_details['s_u_username']) && !empty($a_user_details['s_u_username']) ):
						echo $a_user_details['s_u_username'] .'.';
				?>
						<br/><a href="<?php echo base_url(); ?>user/logout">Logout</a>
						<br/><a href="<?php echo base_url(); ?>post/create_form">Create Post</a>
						<br/><a href="<?php echo base_url(); ?>post/read_all">Search Post</a>
				<?php
					else:
				?>
						Guest. Kindly
						<a href="<?php echo base_url(); ?>user/login_form">
							Login
						</a>
						or 
						<a href="<?php echo base_url(); ?>user/register_form">
							Register
						</a>
				<?php
					endif;
				?>
			</p>
		</header>
		<div>
			<section>
				<header>
					<h2>
						Recent Posts
					</h2>
				</header>
				<?php
					if( isset($a_posts_result) && !empty($a_posts_result) ):
				?>
					<article>
					<?php
						foreach( $a_posts_result AS $a_post_details ):
					?>
						<?php
							if(isset($a_post_details['s_p_title']) && !empty($a_post_details['s_p_title'])):
						?>
						<h3>
							<a href="<?php echo base_url(); ?>post/<?php echo $a_post_details['s_p_slug']; ?>">
								<?php echo $a_post_details['s_p_title']; ?>
							</a>
						</h3>
						<?php
							endif;
						?>
						<?php
							if(isset($a_post_details['s_p_content']) && !empty($a_post_details['s_p_content'])):
						?>
						<p>
							<?php echo $a_post_details['s_p_content']; ?>
						</p>
						<?php
							endif;
						?>
					<?php
						endforeach;
					?>
					</article>
				<?php
					else:
				?>
					<p>
						No post recorded yet.
					</p>
				<?php
					endif;
				?>
			</section>
		</div>
		<footer>
		</footer>
	</body>
</html>