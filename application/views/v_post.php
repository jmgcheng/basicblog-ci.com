<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Basic Blog</title>
	</head>
	<body>
		<header>
			<h1>
				<a href="<?php echo base_url(); ?>">
					Basic Blog
				</a>
			</h1>
			<p>
				Hello, 
				<?php
					$a_user_details = $this->session->userdata('a_user_details');
					if( isset($a_user_details['s_u_username']) && !empty($a_user_details['s_u_username']) ):
						echo $a_user_details['s_u_username'] .'.';
				?>
						<br/><a href="<?php echo base_url(); ?>user/logout">Logout</a>
						<br/><a href="<?php echo base_url(); ?>post/create_form">Create Post</a>
						<br/><a href="<?php echo base_url(); ?>post/read_all">Search Post</a>
				<?php
					else:
				?>
						Guest. Kindly
						<a href="<?php echo base_url(); ?>user/login_form">
							Login
						</a>
						or 
						<a href="<?php echo base_url(); ?>user/register_form">
							Register
						</a>
				<?php
					endif;
				?>
			</p>
		</header>
		<div>
			<section>
				<header>
					<h2>
						Post
					</h2>
				</header>
				<?php
					if( isset($a_post_details) && !empty($a_post_details) ):
				?>
					<article>

						<?php
							if(isset($a_post_details['s_p_title']) && !empty($a_post_details['s_p_title'])):
						?>
						<h3>
							<a href="<?php echo base_url(); ?>post/<?php echo $a_post_details['s_p_slug']; ?>">
								<?php echo $a_post_details['s_p_title']; ?>
							</a>
						</h3>
						<?php
							endif;
						?>
						<?php
							if(isset($a_post_details['s_p_content']) && !empty($a_post_details['s_p_content'])):
						?>
						<p>
							<?php echo $a_post_details['s_p_content']; ?>
						</p>
						<?php
							endif;
						?>

					</article>

					<section>
						<header>
							<h2>
								Add Comment
							</h2>
						</header>
						<form id="frm_post_comment_create" name="frm_post_comment_create" action="<?php echo base_url(); ?>post/<?php echo $a_post_details['s_p_slug'] ?>" method="post" >
							<label for="txt_post_comment_create_content">
								Comment:
							</label>
							<textarea id="txt_post_comment_create_content" name="txt_post_comment_create_content"></textarea>
							<?php if( isset($a_form_notice['s_txt_post_comment_create_content_error']) && !empty($a_form_notice['s_txt_post_comment_create_content_error']) ) : ?>
								<p><?php echo $a_form_notice['s_txt_post_comment_create_content_error']; ?></p>
							<?php endif; ?>
							<br/>
							<input name="btn_post_comment_create" type="submit" value="Post" />
							<br/>
							<?php if( isset($a_form_notice['a_site_response_info']) && !empty($a_form_notice['a_site_response_info']) ) : ?>
								<ul>
								<?php
									foreach( $a_form_notice['a_site_response_info'] AS $s_site_response_info ):
								?>
									<li>
										<?php
											echo $s_site_response_info;
										?>
									</li>
								<?php
									endforeach;
								?>
								</ul>
							<?php endif; ?>	
						</form>
					</section>
					
					<section>
						<header>
							<h2>
								Comments
							</h2>
						</header>
						<?php
							if( isset($a_post_comments) && !empty($a_post_comments) ):
								foreach( $a_post_comments AS $a_post_details ):
						?>
								<div>
									comment on 
									<?php echo $a_post_details['s_pc_date_registration']; ?>
									<br/>
									<?php echo $a_post_details['s_pc_content']; ?>
								</div>
						<?php
								endforeach;
							else:
						?>
							<p>No comments yet.</p>
						<?php
							endif;
						?>
					</section>
					
				<?php
					else:
				?>
					<p>
						Post NOT found.
					</p>
				<?php
					endif;
				?>
			</section>
			
		</div>
		<footer>
		</footer>
	</body>
</html>