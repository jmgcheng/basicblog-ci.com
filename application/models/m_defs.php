<?php
class m_defs extends CI_Model{
	
	
	/**
	* create_data
	*
	* @desc 		a default way to insert data in a database table
	* @param 1 		a_params
	* @return 		array
	* @ex usage
					EG 1.
						$a_new_data = array();
						$a_new_data['s_date_registration'] = date('Y-m-d H:i:s');
						$a_new_data['s_email'] = $_POST['txt_admin_create_user_email'];
						$a_new_data['s_username'] = $_POST['txt_admin_create_user_username']; 
						$a_new_data['s_password'] = $_POST['txt_admin_create_user_password'];
						$a_new_data['s_firstname'] = $_POST['txt_admin_create_user_firstname'];
						$a_new_data['s_lastname'] = $_POST['txt_admin_create_user_lastname'];
						$a_new_data['s_unique_key'] = $s_unique_key;
						$a_new_data['i_usn_id'] = $_POST['opt_admin_create_user_status'];
						$a_add_user_result = array();
						$a_add_user_params['s_table_name'] = 'users';
						$a_add_user_params['a_new_data'] = $a_new_data;
						$a_add_user_result = $this->l_def_sql->create_data( $a_add_user_params );
						if( 	isset($a_add_user_result) && !empty($a_add_user_result) 
							&&	array_key_exists("i_sql_result", $a_add_user_result)
							&& 	$a_add_user_result["i_sql_result"] == 1 
						)
						
	**/
	public function create_data( $a_params = array() )
	{
		$a_result = array();
		
		if( 	isset($a_params['s_table_name']) && !empty($a_params['s_table_name']) 
			&& 	isset($a_params['a_new_data']) && !empty($a_params['a_new_data']) 
		)
		{
			$i_result = $this->db->insert( $a_params['s_table_name'], $a_params['a_new_data'] ); 
			$a_result['i_sql_result'] = $i_result;
			$a_result['i_insert_id'] = $this->db->insert_id();
		}

		return $a_result;
	}
	
	
	/**
	* create_batch_data
	*
	* @desc 		a default way to insert batch data in a database table
	* @param 1 		a_params
	* @return 		array
	* @ex usage
					EG 1.
						$a_add_user_roles_param = array();
						for( $i_counter = 0; $i_counter < count($_POST['chk_admin_create_user_roles']); $i_counter++ )
						{
							$a_add_user_roles_template = array();
							$a_add_user_roles_template['i_u_id'] = $a_add_user_result['i_insert_id'];
							$a_add_user_roles_template['i_urn_id'] = $_POST['chk_admin_create_user_roles'][$i_counter];
							array_push( $a_add_user_roles_param, $a_add_user_roles_template );
						}
						$a_add_user_roles = array();
						$a_add_user_roles['a_new_data'] = $a_add_user_roles_param;
						$a_add_user_roles['s_table_name'] = 'user_roles';
						$a_add_user_roles_result = $this->l_def_sql->create_batch_data( $a_add_user_roles );
						if( 	isset($a_add_user_roles_result) && !empty($a_add_user_roles_result) 
								&&	array_key_exists("i_sql_result", $a_add_user_roles_result)
								&& 	$a_add_user_roles_result["i_sql_result"] == 1 
						)
						
	**/
	public function create_batch_data( $a_params = array() )
	{
		$a_result = array();
		
		if( 	isset($a_params['s_table_name']) && !empty($a_params['s_table_name']) 
			&& 	isset($a_params['a_new_data']) && !empty($a_params['a_new_data']) 
		)
		{
			$this->db->insert_batch( $a_params['s_table_name'], $a_params['a_new_data'] ); 
			$a_result['i_sql_result'] = 1;
		}
		
		return $a_result;
	}
	
	
	/**
	* read_data
	*
	* @desc 		a default way to read data in a database table
	* @param 1 		a_params
	* @return 		array
	* @ex usage
					EG 1.
						$a_u_query_where = array();
						$a_u_query_table_join = array();
						$a_u_query_order_by = array();
						$a_u_query_limit = array();
						$a_u_query_params = array();
						if( isset( $a_assoc_uri['status'] ) && !empty( $a_assoc_uri['status'] ) )
						{
							array_push( $a_u_query_where, array( 's_field' => 'users.i_usn_id', 'a_data' => $a_assoc_uri['status'] ) );
						}
						array_push( $a_u_query_table_join, array( 's_table_join_name' => 'user_status_names', 's_table_join_condition' => 'user_status_names.i_id = users.i_usn_id' ) );
						array_push( $a_u_query_order_by, array( 's_field' => $a_assoc_uri['sort'], 'a_data' => $a_assoc_uri['order'] ) );
						$a_u_query_limit = array('i_limit' => $a_assoc_uri['limit'], 'i_offset' => $a_assoc_uri['offset']);
						$a_u_query_params['a_where'] = $a_u_query_where;
						$a_u_query_params['a_order_by'] = $a_u_query_order_by;
						$a_u_query_params['a_limit'] = $a_u_query_limit;
						$a_u_query_params['a_table_join'] = $a_u_query_table_join;
						$a_u_query_params['s_table_fields'] = $this->m_def_table_fields->s_users_fields . ', ' . $this->m_def_table_fields->s_user_status_names_fields;
						$a_u_query_params['s_table_name'] = 'users';
						$a_user_result = $this->l_def_sql->read_data( $a_u_query_params );
						$a_u_query_params['b_count'] = true;
						$a_user_count_result = $this->l_def_sql->read_data( $a_u_query_params );
						
					EG 2.
						$a_user_roles_query_where = array();
						$a_user_roles_query_where_in = array();
						$a_user_roles_query_table_join = array();
						$a_user_roles_query_params = array();
						array_push( $a_user_roles_query_where_in, array( 's_field' => 'user_roles.i_u_id', 'a_data' => $a_showing_u_id ) );
						array_push( $a_user_roles_query_table_join, array( 's_table_join_name' => 'user_role_names', 's_table_join_condition' => 'user_role_names.i_id = user_roles.i_urn_id' ) );
						$a_user_roles_query_params['a_where_in'] = $a_user_roles_query_where_in;
						$a_user_roles_query_params['a_table_join'] = $a_user_roles_query_table_join;
						$a_user_roles_query_params['s_table_fields'] = $this->m_def_table_fields->s_user_roles_fields . ', ' . $this->m_def_table_fields->s_user_role_names_fields;
						$a_user_roles_query_params['s_table_name'] = 'user_roles';
						$a_user_roles_result = $this->l_def_sql->read_data( $a_user_roles_query_params );
						
					EG 3.
						$a_u_row_result = array();
						$a_u_query_where = array();
						$a_u_query_where_string = array();
						$a_u_query_limit = array();
						$a_u_query_params = array();
						array_push( $a_u_query_where, array( 's_field' => 'users.s_password', 'a_data' => $a_params['s_u_password'] ) );
						array_push( $a_u_query_where_string, "(users.s_email = '" . $a_params['s_u_email_or_username'] . "' OR users.s_username = '" . $a_params['s_u_email_or_username'] . "')" );
						$a_u_query_limit = array('i_limit' => 1, 'i_offset' => 0);
						$a_u_query_params['a_where'] = $a_u_query_where;
						$a_u_query_params['a_where_string'] = $a_u_query_where_string;
						$a_u_query_params['a_limit'] = $a_u_query_limit;
						$a_u_query_params['s_table_fields'] = $this->CI->m_def_table_fields->s_users_fields;
						$a_u_query_params['s_table_name'] = 'users';
						$a_u_result = $this->CI->l_def_sql->read_data( $a_u_query_params );
						if( isset($a_u_result) && !empty($a_u_result) )
						{ $a_u_row_result = $a_u_result[0]; }

	**/
	public function read_data( $a_params = array() )
	{
		$a_result = array();
		
		if( 
				isset($a_params['s_table_fields']) && !empty($a_params['s_table_fields']) 
			&&	isset($a_params['s_table_name']) && !empty($a_params['s_table_name']) 
		)
		{
			if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
			{
				foreach( $a_params['a_where'] AS $a_where_details )
				{
					$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
				}
			}
			
			if( isset($a_params['a_where_in']) && !empty($a_params['a_where_in']) )
			{
				foreach( $a_params['a_where_in'] AS $a_where_in_details )
				{
					$this->db->where_in( $a_where_in_details['s_field'], $a_where_in_details['a_data'] );
				}
			}
			
			if( isset($a_params['a_where_string']) && !empty($a_params['a_where_string']) )
			{
				foreach( $a_params['a_where_string'] AS $a_where_string_details )
				{
					$this->db->where( $a_where_string_details );
				}
			}
			
			if( isset($a_params['b_count']) && !empty($a_params['b_count']) )
			{}
			else
			{
				if( isset($a_params['a_order_by']) && !empty($a_params['a_order_by']) )
				{
					foreach( $a_params['a_order_by'] AS $s_order_by_details )
					{
						$this->db->order_by( $s_order_by_details['s_field'], $s_order_by_details['a_data'] );
					}
				}
			}
			
			if( isset($a_params['b_count']) && !empty($a_params['b_count']) )
			{}
			else
			{
				if( isset($a_params['a_limit']) && !empty($a_params['a_limit']) )
				{
					$this->db->limit( $a_params['a_limit']['i_limit'], $a_params['a_limit']['i_offset']);
				}
			}
			
			
			$this->db->select( $a_params['s_table_fields'] );
			$this->db->from( $a_params['s_table_name'] );
			
			if( isset($a_params['a_table_join']) && !empty($a_params['a_table_join']) )
			{
				foreach( $a_params['a_table_join'] AS $a_table_join_details )
				{
					$this->db->join( $a_table_join_details['s_table_join_name'], $a_table_join_details['s_table_join_condition'] );
				}
			}
			
			$o_query_result = $this->db->get();
			/*
				$o_query_result = $this->db->get( $a_params['s_table_name'] );
				echo $this->db->last_query();
			*/
			
			
			if( isset($a_params['b_count']) && !empty($a_params['b_count']) )
			{
				$a_result['i_num_rows'] = 0;
				$a_result['i_num_rows'] = $o_query_result->num_rows();
			}
			else
			{
				$a_result = $o_query_result->result_array();
			}

		}

		return $a_result;
	}
	
	
	/**
	* update_data
	*
	* @desc 		a default way to update data in a database table
	* @param 1 		a_params
	* @return 		array
	* @ex usage
					EG 1.
						$a_update_u_query_where = array();
						$a_update_u_query_params = array();
						array_push( $a_update_u_query_where, array( 's_field' => 'users.i_id', 'a_data' => $a_user_details['i_u_id'] ) );
						$a_update_data = array();
						$a_update_data['s_email'] = $_POST['txt_account_update_profile_email'];
						$a_update_data['s_username'] = $_POST['txt_account_update_profile_username'];
						$a_update_data['s_firstname'] = $_POST['txt_account_update_profile_firstname'];
						$a_update_data['s_lastname'] = $_POST['txt_account_update_profile_lastname'];
						$a_update_u_query_params['a_where'] = $a_update_u_query_where;
						$a_update_u_query_params['a_update_data'] = $a_update_data;
						$a_update_u_query_params['s_table_name'] = 'users';
						$a_update_u_result = $this->l_def_sql->update_data( $a_update_u_query_params );
						if( 	isset($a_update_u_result) && !empty($a_update_u_result) 
							&&	array_key_exists("i_sql_result", $a_update_u_result)
							&& 	$a_update_u_result["i_sql_result"] == 1 
						)
						
	**/
	public function update_data( $a_params = array() )
	{
		$a_result = array();
		
		if( 
				isset($a_params['a_update_data']) && !empty($a_params['a_update_data']) 
			&&	isset($a_params['a_where']) && !empty($a_params['a_where']) 
			&&	isset($a_params['s_table_name']) && !empty($a_params['s_table_name']) 
		)
		{
			if( isset($a_params['a_where']) && !empty($a_params['a_where']) )
			{
				foreach( $a_params['a_where'] AS $a_where_details )
				{
					$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
				}
			}

			$this->db->update( $a_params['s_table_name'] , $a_params['a_update_data'] ); 
			$a_result['i_sql_result'] = 1;
			
		}

		return $a_result;
	}
	
	
	/**
	* delete_data
	*
	* @desc 		a default way to delete data in a database table
	* @param 1 		a_params
	* @return 		array
	* @ex usage
					EG1
						$a_delete_user_status_names_query_where = array();
						$a_delete_user_status_names_query_params = array();
						array_push( $a_delete_user_status_names_query_where, array( 's_field' => 'user_status_names.i_id', 'a_data' => $a_assoc_uri['user_status_name_id'] ) );
						$a_delete_user_status_names_query_params['a_where'] = $a_delete_user_status_names_query_where;
						$a_delete_user_status_names_query_params['s_table_name'] = 'user_status_names';
						$a_delete_user_status_names_result = $this->l_def_sql->delete_data( $a_delete_user_status_names_query_params );	
						if( 	isset($a_delete_user_status_names_result) && !empty($a_delete_user_status_names_result) 
						&&	array_key_exists("i_sql_result", $a_delete_user_status_names_result)
						&& 	$a_delete_user_status_names_result["i_sql_result"] == 1 
						)

	**/
	public function delete_data( $a_params = array() )
	{
		$a_result = array();

		if( 
				isset($a_params['a_where']) && !empty($a_params['a_where']) 
			&&	isset($a_params['s_table_name']) && !empty($a_params['s_table_name']) 
		)
		{
			foreach( $a_params['a_where'] AS $a_where_details )
			{
				$this->db->where( $a_where_details['s_field'], $a_where_details['a_data'] );
			}

			$i_result = $this->db->delete( $a_params['s_table_name'] ); 
			$a_result['i_sql_result'] = $i_result;
		}

		return $a_result;
	}
	
}