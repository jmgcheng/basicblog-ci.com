<?php
class m_posts extends CI_Model{
	
	public $s_posts_fields = 	'posts.i_id AS i_p_id, 
								posts.i_u_id AS i_p_u_id, 
								posts.s_date_registration AS s_p_date_registration, 
								posts.s_title AS s_p_title, 
								posts.s_slug AS s_p_slug, 
								posts.s_content AS s_p_content,
								posts.i_psn_id AS i_p_psn_id';
								
	public $s_post_status_names_fields = 	'post_status_names.i_id AS i_psn_id,
											post_status_names.s_name AS s_psn_name';
											
}