<?php
class m_users extends CI_Model{
	
	public $s_users_fields = 	'users.i_id AS i_u_id, 
								users.s_date_registration AS s_u_date_registration, 
								users.s_email AS s_u_email, 
								users.s_username AS s_u_username, 
								users.s_firstname AS s_u_firstname, 
								users.s_lastname AS s_u_lastname, 
								users.s_password AS s_u_password, 
								users.s_unique_key AS s_u_unique_key, 
								users.i_usn_id AS i_u_usn_id';
	
	public $s_user_status_names_fields = 	'user_status_names.i_id AS i_usn_id,
											user_status_names.s_name AS s_usn_name';
	
}