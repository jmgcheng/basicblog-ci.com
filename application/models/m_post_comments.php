<?php
class m_post_comments extends CI_Model{
	
	public $s_post_comments_fields = 	'post_comments.i_id AS i_pc_id, 
										post_comments.i_p_id AS i_pc_p_id, 
										post_comments.i_u_id AS i_pc_u_id, 
										post_comments.s_date_registration AS s_pc_date_registration, 
										post_comments.s_content AS s_pc_content, 
										post_comments.i_pcsn_id AS i_pc_pcsn_id';

										
	public $s_post_comment_status_names_fields = 	'post_comment_status_names.i_id AS i_pcsn_id,
													 post_comment_status_names.s_name AS s_pcsn_name';

													 
}