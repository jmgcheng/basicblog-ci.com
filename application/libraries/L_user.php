<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class L_user {

	/**
	* constructor
	* @desc		
	*
	**/
	public function __construct()
	{
		$this->CI =& get_instance();
	}
	
	
	/**
	* login
	*
	* @desc 	
	* @param 1 	a_params
	* @return 	array
	*
	**/
	public function login( $a_params = array() )
	{
		// ========================================================
		$a_result = array();
		$a_result['i_result'] = 0;
		// ========================================================
		
		
		// ========================================================
		$this->CI->load->model('m_users');
		$this->CI->load->model('m_defs');
		
		// ========================================================

		
		// ========================================================
		if( !empty($a_params) && !empty($a_params['s_u_email_or_username']) && !empty($a_params['s_u_password']) )
		{
			$a_u_row_result = array();
			$a_u_query_where = array();
			$a_u_query_where_string = array();
			$a_u_query_limit = array();
			$a_u_query_params = array();
			array_push( $a_u_query_where, array( 's_field' => 'users.s_password', 'a_data' => $a_params['s_u_password'] ) );
			array_push( $a_u_query_where_string, "(users.s_email = '" . $a_params['s_u_email_or_username'] . "' OR users.s_username = '" . $a_params['s_u_email_or_username'] . "')" );
			$a_u_query_limit = array('i_limit' => 1, 'i_offset' => 0);
			$a_u_query_params['a_where'] = $a_u_query_where;
			$a_u_query_params['a_where_string'] = $a_u_query_where_string;
			$a_u_query_params['a_limit'] = $a_u_query_limit;
			$a_u_query_params['s_table_fields'] = $this->CI->m_users->s_users_fields;
			$a_u_query_params['s_table_name'] = 'users';
			$a_u_result = $this->CI->m_defs->read_data( $a_u_query_params );
			if( isset($a_u_result) && !empty($a_u_result) )
			{ $a_u_row_result = $a_u_result[0]; }
			

			if( !empty($a_u_row_result) )
			{
				if( $a_u_row_result['i_u_usn_id'] == 1 ) // by default, 1 is Activated
				{
					$a_session_user_details = array(
						'a_user_details'  => $a_u_row_result,
						'b_is_user_login'  => TRUE
					);
					
					$this->CI->session->set_userdata($a_session_user_details);

					$a_result['i_result'] = 1;
				}
				elseif( $a_u_row_result['i_u_usn_id'] == 2 ) // by default
				{
					$a_result['s_message_status'] = 'info';
					$a_result['s_message_notice'] = 'Email Activation Required';
				}
				elseif( $a_u_row_result['i_u_usn_id'] == 3 ) // by default
				{
					$a_result['s_message_status'] = 'info';
					$a_result['s_message_notice'] = 'Deactivated';
				}
				elseif( $a_u_row_result['i_u_usn_id'] == 4 ) // by default
				{
					$a_result['s_message_status'] = 'error';
					$a_result['s_message_notice'] = 'Banned';
				}
				elseif( $a_u_row_result['i_u_usn_id'] == 5 ) // by default
				{
					$a_result['s_message_status'] = 'info';
					$a_result['s_message_notice'] = 'Lock - Max Failed Login';
				}
				else
				{
					$a_result['s_message_status'] = 'info';
					$a_result['s_message_subject'] = 'Login';
					$a_result['s_message_notice'] = 'Account NOT activated';
				}
			}
			else
			{
				$a_result['s_message_status'] = 'error';
				$a_result['s_message_subject'] = 'Login';
				$a_result['s_message_notice'] = 'Incorrect Details';
			}
		}

		return $a_result;
		// ========================================================
	}
	
	
}