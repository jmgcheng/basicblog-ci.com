<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_home extends CI_Controller {

	
	/**
	* contructor
	* @desc		
	*
	**/
	public function __construct()
	{
		parent::__construct();
	}
	
	
	/**
	* index
	* @desc		
	*
	**/
	public function index()
	{
		//======================================================
		$this->load->model('m_posts');
		$this->load->model('m_defs');
		//======================================================
		
		
		/*
			Get posts
		*/
		$a_posts_query_where = array();
		$a_posts_query_order_by = array();
		$a_posts_query_limit = array();
		$a_posts_query_params = array();
		array_push( $a_posts_query_where, array( 's_field' => 'posts.i_psn_id', 'a_data' => 1 ) );
		array_push( $a_posts_query_order_by, array( 's_field' => 'posts.s_date_registration', 'a_data' => 'DESC' ) );
		$a_posts_query_limit = array('i_limit' => 10, 'i_offset' => 0);
		$a_posts_query_params['a_where'] = $a_posts_query_where;
		$a_posts_query_params['a_order_by'] = $a_posts_query_order_by;
		$a_posts_query_params['a_limit'] = $a_posts_query_limit;
		$a_posts_query_params['s_table_fields'] = $this->m_posts->s_posts_fields;
		$a_posts_query_params['s_table_name'] = 'posts';
		$a_posts_result = $this->m_defs->read_data( $a_posts_query_params );
		
		
		$a_view_data = array();
		$a_view_data['a_posts_result'] = $a_posts_result;
		$this->load->view('v_home', $a_view_data);
	}
}
