<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_post extends CI_Controller {


	/**
	* contructor
	* @desc		
	*
	**/
	public function __construct()
	{
		parent::__construct();
	}
	
	
	/**
	* index
	* @desc		
	*
	**/
	public function index( $s_slug = '' )
	{
		//= Declare Start-Up Variables Here ====================
		$a_form_notice = array();
		$a_site_response_error = array();
		$a_site_response_info = array();
		//======================================================
		
		
		//======================================================
		$this->load->model('m_posts');
		$this->load->model('m_post_comments');
		$this->load->model('m_defs');
		$this->load->library('form_validation');
		$this->load->helper(array('form', 'security'));
		//======================================================
		
		
		//======================================================
		if( !isset($s_slug) || empty($s_slug) )
		{ redirect( base_url() ); }
		//======================================================
		
		
		/*
			Get posts
		*/
		$a_posts_query_where = array();
		$a_posts_query_limit = array();
		$a_posts_query_params = array();
		array_push( $a_posts_query_where, array( 's_field' => 'posts.s_slug', 'a_data' => $s_slug ) );
		$a_posts_query_limit = array('i_limit' => 1, 'i_offset' => 0);
		$a_posts_query_params['a_where'] = $a_posts_query_where;
		$a_posts_query_params['a_limit'] = $a_posts_query_limit;
		$a_posts_query_params['s_table_fields'] = $this->m_posts->s_posts_fields;
		$a_posts_query_params['s_table_name'] = 'posts';
		$a_posts_result = $this->m_defs->read_data( $a_posts_query_params );
		$a_post_details = array();
		if( isset($a_posts_result[0]) && !empty($a_posts_result[0]) )
		{
			$a_post_details = $a_posts_result[0];
		}
		
		
		/*
			Get posts comments
		*/
		$a_post_comment_query_where = array();
		$a_post_comment_query_order_by = array();
		$a_post_comment_query_params = array();
		array_push( $a_post_comment_query_where, array( 's_field' => 'post_comments.i_p_id', 'a_data' => $a_post_details['i_p_id'] ) );
		array_push( $a_post_comment_query_order_by, array( 's_field' => 'post_comments.s_date_registration', 'a_data' => 'DESC' ) );
		$a_post_comment_query_params['a_where'] = $a_post_comment_query_where;
		$a_post_comment_query_params['a_order_by'] = $a_post_comment_query_order_by;
		$a_post_comment_query_params['s_table_fields'] = $this->m_post_comments->s_post_comments_fields;
		$a_post_comment_query_params['s_table_name'] = 'post_comments';
		$a_post_comments = array();
		$a_post_comment_result = $this->m_defs->read_data( $a_post_comment_query_params );
		$a_post_comments = $a_post_comment_result;
		
		
		if( isset($_POST) && !empty($_POST) )
		{
			$this->form_validation->set_rules('txt_post_comment_create_content', 'Comment', 'trim|required|min_length[5]|max_length[255]|xss_clean');
			
			if ($this->form_validation->run() == FALSE)
			{
				$a_form_notice['s_txt_post_comment_create_content_error'] = form_error('txt_post_comment_create_content', ' ', ' ');
				if( isset($a_form_notice['s_txt_post_comment_create_content_error']) && !empty($a_form_notice['s_txt_post_comment_create_content_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_post_comment_create_content_error'] );
				}
				$a_form_notice['a_site_response_error'] = $a_site_response_error;
			}
			else
			{
				$a_user_details = $this->session->userdata('a_user_details');
				if( !isset($a_user_details) || empty($a_user_details) )
				{ redirect( base_url() . 'user/logout', 'refresh'); }
				
				
				$a_new_data = array();
				$a_new_data['i_p_id'] = $a_post_details['i_p_id'];
				$a_new_data['i_u_id'] = $a_user_details['i_u_id'];
				$a_new_data['s_date_registration'] = date('Y-m-d H:i:s');
				$a_new_data['s_content'] = $_POST['txt_post_comment_create_content'];
				$a_new_data['i_pcsn_id'] = 1;
				
				$a_add_post_comment_result = array();
				$a_add_post_comment_params['s_table_name'] = 'post_comments';
				$a_add_post_comment_params['a_new_data'] = $a_new_data;
				
				$a_add_post_comment_result = $this->m_defs->create_data( $a_add_post_comment_params );
				
				if( 	isset($a_add_post_comment_result) && !empty($a_add_post_comment_result) 
					&&	array_key_exists("i_sql_result", $a_add_post_comment_result)
					&& 	$a_add_post_comment_result["i_sql_result"] == 1 
				)
				{
					array_push( $a_site_response_info, 'Comment Added' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
				}
				else
				{
					array_push( $a_site_response_info, 'Comment NOT Added. Please Contact Administrator' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
				}
			}
		}
		
		
		$a_view_data = array();
		$a_view_data['a_form_notice'] = $a_form_notice;
		$a_view_data['a_post_details'] = $a_post_details;
		$a_view_data['a_post_comments'] = $a_post_comments;
		$this->load->view('v_post', $a_view_data);
	}

	
	/**
	* create_form
	* @desc		
	*
	**/
	public function create_form()
	{
		//= Declare Start-Up Variables Here ====================
		$a_form_notice = array();
		$a_site_response_error = array();
		$a_site_response_info = array();
		//======================================================
		
		
		//======================================================
		$this->load->model('m_defs');
		$this->load->library('form_validation');
		$this->load->helper(array('form', 'security'));
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		if( !isset($a_user_details) || empty($a_user_details) )
		{ redirect( base_url() . 'user/logout', 'refresh'); }
		//======================================================
		
		
		if( isset($_POST) && !empty($_POST) )
		{
			$this->form_validation->set_rules('txt_post_create_title', 'Title', 'trim|required|min_length[5]|max_length[55]|xss_clean|is_unique[posts.s_title]');
			$this->form_validation->set_rules('txt_post_create_content', 'Content', 'trim|required|xss_clean');
						
			if ($this->form_validation->run() == FALSE)
			{
				$a_form_notice['s_txt_post_create_title_error'] = form_error('txt_post_create_title', ' ', ' ');
				if( isset($a_form_notice['s_txt_post_create_title_error']) && !empty($a_form_notice['s_txt_post_create_title_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_post_create_title_error'] );
				}
				$a_form_notice['s_txt_post_create_content_error'] = form_error('txt_post_create_content', ' ', ' ');
				if( isset($a_form_notice['s_txt_post_create_content_error']) && !empty($a_form_notice['s_txt_post_create_content_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_post_create_content_error'] );
				}
				
				$a_form_notice['a_site_response_error'] = $a_site_response_error;
			}
			else
			{
				$s_slug = $_POST['txt_post_create_title'];
				// replace non letter or digits by -
				$s_slug = preg_replace('~[^\\pL\d]+~u', '-', $s_slug);
				// trim
				$s_slug = trim($s_slug, '-');
				// transliterate
				$s_slug = iconv('utf-8', 'us-ascii//TRANSLIT', $s_slug);
				// lowercase
				$s_slug = strtolower($s_slug);
				// remove unwanted characters
				$s_slug = preg_replace('~[^-\w]+~', '', $s_slug);
				
				$a_new_data = array();
				$a_new_data['i_u_id'] = $a_user_details['i_u_id'];
				$a_new_data['s_date_registration'] = date('Y-m-d H:i:s');
				$a_new_data['s_title'] = $_POST['txt_post_create_title'];
				$a_new_data['s_slug'] = $s_slug; 
				$a_new_data['s_content'] = $_POST['txt_post_create_content'];
				$a_new_data['i_psn_id'] = 1;
				
				$a_add_post_result = array();
				$a_add_user_params['s_table_name'] = 'posts';
				$a_add_user_params['a_new_data'] = $a_new_data;
				
				$a_add_post_result = $this->m_defs->create_data( $a_add_user_params );
				
				if( 	isset($a_add_post_result) && !empty($a_add_post_result) 
					&&	array_key_exists("i_sql_result", $a_add_post_result)
					&& 	$a_add_post_result["i_sql_result"] == 1 
				)
				{
					array_push( $a_site_response_info, 'Post Added Successfully' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
				}
				else
				{
					array_push( $a_site_response_info, 'Post NOT Added. Please Contact Administrator' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
				}
			}
		}
		
		
		$a_view_data = array();
		$a_view_data['a_form_notice'] = $a_form_notice;
		$this->load->view('v_posts_create_form', $a_view_data);
	}
	
	
	/**
	* read_all
	* @desc		
	*
	**/
	public function read_all()
	{
		//= Declare Start-Up Variables Here ====================
		$a_form_notice = array();
		$a_site_response_error = array();
		$a_site_response_info = array();
		//======================================================
		
		
		//======================================================
		$this->load->model('m_defs');
		$this->load->model('m_posts');
		$this->load->library('form_validation');
		$this->load->library('pagination');
		$this->load->helper(array('form', 'security'));
		//======================================================
		
		
		/*
			parse uri first
		*/
		$a_expected_uri = array('limit', 'offset', 'sort', 'order', 'search');
		$a_assoc_uri = $this->uri->uri_to_assoc(3, $a_expected_uri);
		
		
		/*
		*/
		if( !isset($a_assoc_uri['limit']) || empty($a_assoc_uri['limit']) )
		{ $a_assoc_uri['limit'] = 2; }
		if( !isset($a_assoc_uri['offset']) || empty($a_assoc_uri['offset']) )
		{ $a_assoc_uri['offset'] = 0; }
		if( !isset($a_assoc_uri['sort']) || empty($a_assoc_uri['sort']) )
		{ $a_assoc_uri['sort'] = 's_date_registration'; }
		if( !isset($a_assoc_uri['order']) || empty($a_assoc_uri['order']) )
		{ $a_assoc_uri['order'] = 'desc'; }
		if( isset($_POST['txt_post_search_title']) && !empty($_POST['txt_post_search_title']) )
		{ $a_assoc_uri['search'] = $_POST['txt_post_search_title']; }
		if( !isset($a_assoc_uri['search']) || empty($a_assoc_uri['search']) )
		{ $a_assoc_uri['search'] = '-'; }
	
		
		/*
		*/
		$a_posts_query_where = array();
		$a_posts_query_where_string = array();
		$a_posts_query_order_by = array();
		$a_posts_query_limit = array();
		$a_posts_query_params = array();
		array_push( $a_posts_query_where, array( 's_field' => 'posts.i_psn_id', 'a_data' => 1 ) );
		if( isset($a_assoc_uri['search']) && !empty($a_assoc_uri['search']) && $a_assoc_uri['search'] != '-' )
		{
			array_push( $a_posts_query_where_string, "(posts.s_title LIKE '%" . $a_assoc_uri['search'] . "%' OR posts.s_content LIKE '%" . $a_assoc_uri['search'] . "%')" );
		}
		array_push( $a_posts_query_order_by, array( 's_field' => $a_assoc_uri['sort'], 'a_data' => $a_assoc_uri['order'] ) );
		$a_posts_query_limit = array('i_limit' => $a_assoc_uri['limit'], 'i_offset' => $a_assoc_uri['offset']);
		$a_posts_query_params['a_where'] = $a_posts_query_where;
		$a_posts_query_params['a_where_string'] = $a_posts_query_where_string;
		$a_posts_query_params['a_order_by'] = $a_posts_query_order_by;
		$a_posts_query_params['a_limit'] = $a_posts_query_limit;
		$a_posts_query_params['s_table_fields'] = $this->m_posts->s_posts_fields;
		$a_posts_query_params['s_table_name'] = 'posts';
		
		
		/*
		*/
		$a_post_results = $this->m_defs->read_data( $a_posts_query_params );
		$a_posts_query_params['b_count'] = true;
		$a_post_count_result = $this->m_defs->read_data( $a_posts_query_params );
		
		
		/*
			Paging
		*/
		$i_page_uri_segment = 12;
		$a_pagination_config['base_url'] = base_url() . 'post/read_all/limit/' .$a_assoc_uri['limit']. '/order/' .$a_assoc_uri['order']. '/sort/' .$a_assoc_uri['sort']. '/search/' .$a_assoc_uri['search']. '/offset/';
		$a_pagination_config['total_rows'] = $a_post_count_result['i_num_rows'];
		$a_pagination_config['per_page'] = $a_assoc_uri['limit'];
		$a_pagination_config['uri_segment'] = $i_page_uri_segment;
		$a_pagination_config['num_links'] = 5;
		$a_pagination_config['first_link'] = false;
		$a_pagination_config['last_link'] = false;
		$a_pagination_config['num_tag_open'] = '<li>';
		$a_pagination_config['num_tag_close'] = '</li>';
		$a_pagination_config['cur_tag_open'] = '<li class="clsli_pageactive_1"><a href="#" onclick="return false;">';
		$a_pagination_config['cur_tag_close'] = '</a></li>';
		$a_pagination_config['next_link'] = 'Next Page';
		$a_pagination_config['next_tag_open'] = '<li>';
		$a_pagination_config['next_tag_close'] = '</li>';
		$a_pagination_config['prev_link'] = 'Prev Page';
		$a_pagination_config['prev_tag_open'] = '<li>';
		$a_pagination_config['prev_tag_close'] = '</li>';
		$a_pagination_config['full_tag_open'] = '<ul>';
		$a_pagination_config['full_tag_close'] = '</ul>';
		$this->pagination->initialize($a_pagination_config); 
		$s_page_links_pagination = $this->pagination->create_links();
		
		
		$a_view_data = array();
		$a_view_data['a_assoc_uri'] = $a_assoc_uri;
		$a_view_data['a_post_results'] = $a_post_results;
		$a_view_data['a_post_count_result'] = $a_post_count_result;
		$a_view_data['s_page_links_pagination'] = $s_page_links_pagination;
		
		$a_view_data['a_form_notice'] = $a_form_notice;
		$this->load->view('v_posts_read_all', $a_view_data);
	}
	
}
