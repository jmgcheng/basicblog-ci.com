<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_user extends CI_Controller {


	/**
	* contructor
	* @desc		
	*
	**/
	public function __construct()
	{
		parent::__construct();
	}
	
	
	/**
	* index
	* @desc		
	*
	**/
	public function index()
	{}

	
	/**
	* register_form
	* @desc		
	*
	**/
	public function register_form()
	{
		//= Declare Start-Up Variables Here ====================
		$a_form_notice = array();
		$a_site_response_error = array();
		$a_site_response_info = array();
		//======================================================
		
		
		//======================================================
		$this->load->model('m_defs');
		$this->load->library('form_validation');
		$this->load->helper(array('form', 'security'));
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		if( !empty($a_user_details) )
		{
			redirect( base_url() . 'user/logout', 'refresh');
		}
		//======================================================
		
		
		if( isset($_POST) && !empty($_POST) )
		{
			$this->form_validation->set_rules('txt_user_registration_username', 'Username', 'trim|required|min_length[5]|max_length[12]|xss_clean|is_unique[users.s_username]');
			$this->form_validation->set_rules('txt_user_registration_email', 'Email', 'trim|required|valid_email|xss_clean|is_unique[users.s_email]');
			$this->form_validation->set_rules('txt_user_registration_password', 'Password', 'trim|required|md5|xss_clean');
			
			if ($this->form_validation->run() == FALSE)
			{
				$a_form_notice['s_txt_user_registration_username_error'] = form_error('txt_user_registration_username', ' ', ' ');
				if( isset($a_form_notice['s_txt_user_registration_username_error']) && !empty($a_form_notice['s_txt_user_registration_username_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_user_registration_username_error'] );
				}
				$a_form_notice['s_txt_user_registration_email_error'] = form_error('txt_user_registration_email', ' ', ' ');
				if( isset($a_form_notice['s_txt_user_registration_email_error']) && !empty($a_form_notice['s_txt_user_registration_email_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_user_registration_email_error'] );
				}
				$a_form_notice['s_txt_user_registration_password_error'] = form_error('txt_user_registration_password', ' ', ' ');
				if( isset($a_form_notice['s_txt_user_registration_password_error']) && !empty($a_form_notice['s_txt_user_registration_password_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_user_registration_password_error'] );
				}
				
				$a_form_notice['a_site_response_error'] = $a_site_response_error;
			}
			else
			{
				$s_unique_key = md5( ( strtotime('now') . $_POST['txt_user_registration_username'] ) );
					
				$a_new_data = array();
				$a_new_data['s_date_registration'] = date('Y-m-d H:i:s');
				$a_new_data['s_email'] = $_POST['txt_user_registration_email'];
				$a_new_data['s_username'] = $_POST['txt_user_registration_username']; 
				$a_new_data['s_password'] = $_POST['txt_user_registration_password'];
				$a_new_data['s_unique_key'] = $s_unique_key;
				$a_new_data['i_usn_id'] = 1;
				
				$a_add_user_result = array();
				$a_add_user_params['s_table_name'] = 'users';
				$a_add_user_params['a_new_data'] = $a_new_data;
				
				$a_add_user_result = $this->m_defs->create_data( $a_add_user_params );
				
				if( 	isset($a_add_user_result) && !empty($a_add_user_result) 
					&&	array_key_exists("i_sql_result", $a_add_user_result)
					&& 	$a_add_user_result["i_sql_result"] == 1 
				)
				{
					array_push( $a_site_response_info, 'Successful Registration. You can now login' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
				}
				else
				{
					array_push( $a_site_response_info, 'Unsuccessful Registration. Please Contact Administrator' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
				}
			}
		}
		
		$a_view_data = array();
		$a_view_data['a_form_notice'] = $a_form_notice;
		$this->load->view('v_users_register_form', $a_view_data);
	}
	
	
	/**
	* login_form
	* @desc		
	*
	**/
	public function login_form()
	{
		//= Declare Start-Up Variables Here ====================
		$a_form_notice = array();
		$a_site_response_error = array();
		$a_site_response_info = array();
		//======================================================
		
		
		//======================================================
		$this->load->model('m_defs');
		$this->load->library('l_user');
		$this->load->library('form_validation');
		$this->load->helper(array('form', 'security'));
		//======================================================
		
		
		//======================================================
		$a_user_details = $this->session->userdata('a_user_details');
		if( !empty($a_user_details) )
		{
			redirect( base_url() . 'user/logout', 'refresh');
		}
		//======================================================
		
		
		//======================================================
		if( isset($_POST) && !empty($_POST) )
		{
			$this->form_validation->set_rules('txt_user_login_emailorusername', 'Email or Username', 'trim|required|xss_clean');
			$this->form_validation->set_rules('txt_user_login_password', 'Password', 'trim|required|md5|xss_clean');
			
			if ($this->form_validation->run() == FALSE)
			{
				$a_form_notice['s_txt_user_login_emailorusername_error'] = form_error('txt_user_login_emailorusername', ' ', ' ');
				if( isset($a_form_notice['s_txt_user_login_emailorusername_error']) && !empty($a_form_notice['s_txt_user_login_emailorusername_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_user_login_emailorusername_error'] );
				}
				
				$a_form_notice['s_txt_user_login_password_error'] = form_error('txt_user_login_password', ' ', ' ');
				if( isset($a_form_notice['s_txt_user_login_password_error']) && !empty($a_form_notice['s_txt_user_login_password_error']) )
				{
					array_push( $a_site_response_error, $a_form_notice['s_txt_user_login_password_error'] );
				}

				$a_form_notice['a_site_response_error'] = $a_site_response_error;
			}
			else
			{
				$a_login_params = array();
				$a_login_params['s_u_email_or_username'] = $_POST['txt_user_login_emailorusername'];
				$a_login_params['s_u_password'] = $_POST['txt_user_login_password'];
				
				$a_account_login_result = array();
				$a_account_login_result = $this->l_user->login( $a_login_params );
				
				if( isset($a_account_login_result) && !empty($a_account_login_result) && array_key_exists("i_result", $a_account_login_result) )
				{
					if( $a_account_login_result['i_result'] == 1 )
					{
						redirect( base_url(), 'refresh');
					}
					else
					{
						if( isset($a_account_login_result['s_message_status']) && !empty($a_account_login_result['s_message_status']) 
							&& $a_account_login_result['s_message_status'] == 'info'
						)
						{
							array_push( $a_site_response_info, $a_account_login_result['s_message_notice'] );
							$a_form_notice['a_site_response_info'] = $a_site_response_info;
						}
						elseif( isset($a_account_login_result['s_message_status']) && !empty($a_account_login_result['s_message_status']) 
							&& $a_account_login_result['s_message_status'] == 'error'
						)
						{
							array_push( $a_site_response_error, $a_account_login_result['s_message_notice'] );
							$a_form_notice['a_site_response_error'] = $a_site_response_error;
						}
					}
				}
				else
				{
					array_push( $a_site_response_info, 'Unable to Login. Please Contact Administrator' );
					$a_form_notice['a_site_response_info'] = $a_site_response_info;
				}
			}
		}
		
		
		$a_view_data = array();
		$a_view_data['a_form_notice'] = $a_form_notice;
		$this->load->view('v_users_login_form', $a_view_data);
	}
	
	
	/**
	* logout
	* @desc		
	*
	**/
	public function logout()
	{
		$this->session->sess_destroy();
		redirect( base_url(), 'refresh');
	}
	
	
}
