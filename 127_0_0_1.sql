-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 22, 2016 at 02:09 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `basicblog_ci`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `post_comment_status_names`
--

CREATE TABLE IF NOT EXISTS `post_comment_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(55) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `post_comments`
--

CREATE TABLE IF NOT EXISTS `post_comments` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_p_id` int(11) NOT NULL,
  `i_u_id` int(11) NOT NULL,
  `s_date_registration` datetime NOT NULL,
  `s_content` varchar(255) NOT NULL,
  `i_pcsn_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `post_comments`
--

INSERT INTO `post_comments` (`i_id`, `i_p_id`, `i_u_id`, `s_date_registration`, `s_content`, `i_pcsn_id`) VALUES
(1, 2, 1, '2015-11-16 12:59:32', 'the comment', 1),
(2, 1, 1, '2015-11-16 13:20:13', 'another comment 1', 1),
(3, 1, 1, '2015-11-16 13:20:49', 'another comment 3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `post_status_names`
--

CREATE TABLE IF NOT EXISTS `post_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(55) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `post_status_names`
--

INSERT INTO `post_status_names` (`i_id`, `s_name`) VALUES
(1, 'Published'),
(2, 'Draft'),
(3, 'Trashed');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_u_id` int(11) NOT NULL,
  `s_date_registration` datetime NOT NULL,
  `s_title` varchar(55) NOT NULL,
  `s_slug` varchar(55) NOT NULL,
  `s_content` text,
  `i_psn_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`i_id`, `i_u_id`, `s_date_registration`, `s_title`, `s_slug`, `s_content`, `i_psn_id`) VALUES
(1, 1, '2015-11-16 12:22:56', 'Title 1', 'title-1', 'Content 1', 1),
(2, 1, '2015-11-16 12:23:13', 'The! Best Article issue.', 'the-best-article-issue', 'Content of the best', 1),
(3, 1, '2015-11-16 13:22:11', 'Title 3', 'title-3', 'The quick brown fox jumps over the lazy dog.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_status_names`
--

CREATE TABLE IF NOT EXISTS `user_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(55) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user_status_names`
--

INSERT INTO `user_status_names` (`i_id`, `s_name`) VALUES
(1, 'Activated'),
(2, 'Email Activation Required'),
(3, 'Deactivated'),
(4, 'Banned'),
(5, 'Lock - Max Failed Login');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_date_registration` datetime NOT NULL,
  `s_email` varchar(55) NOT NULL,
  `s_username` varchar(55) NOT NULL,
  `s_firstname` varchar(55) NOT NULL,
  `s_lastname` varchar(55) NOT NULL,
  `s_password` varchar(255) NOT NULL,
  `s_unique_key` varchar(255) NOT NULL,
  `i_usn_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`i_id`, `s_date_registration`, `s_email`, `s_username`, `s_firstname`, `s_lastname`, `s_password`, `s_unique_key`, `i_usn_id`) VALUES
(1, '2015-11-16 09:31:13', 'test3@yahoo.com', 'test3', '', '', '15acb765a111eba6e36cf64d6560e09a', 'a50e274aba28f9d78b8103e0e9eb89ec', 1),
(2, '2016-02-20 14:35:03', 'test1@yahoo.com', 'test1', '', '', '15acb765a111eba6e36cf64d6560e09a', '6a8ed409255edb1caaff05b51535be0c', 1),
(3, '2016-02-20 14:35:23', 'test2@yahoo.com', 'test2', '', '', '15acb765a111eba6e36cf64d6560e09a', '0860fb38b6d403e2b7aa26807f916aa0', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
