# What is this repository for? #

A simple template blog using CodeIgniter version 3.0.3

## How do I get set up? ##

* create a folder in local server
* cd project
* clone repo
* check out "127_0_0_1.sql" to install in the database
* check out config files and adjust setup